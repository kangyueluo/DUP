package com.dup.tdup

import android.support.v7.app.AppCompatActivity
import com.dup.tdup.DatabaseManager
import android.graphics.Bitmap
import android.support.annotation.RequiresApi
import android.os.Build
import android.os.Bundle
import com.dup.tdup.R
import android.content.Intent
import com.dup.tdup.AddOutfitActivity
import android.graphics.drawable.BitmapDrawable
import android.view.Gravity
import android.widget.SeekBar.OnSeekBarChangeListener
import com.dup.tdup.ImageProcessor
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.*
import java.io.IOException

class AddOutfitActivity() : AppCompatActivity() {
    private var imageView: ImageView? = null
    private var picker_btn: Button? = null
    private var insert_btn: Button? = null
    private var sensitivity_bar: SeekBar? = null
    private var database: DatabaseManager? = null
    private var selected_bmp: Bitmap? = null
    private val background_color = intArrayOf(255, 255, 255)
    private val TAG = "A- AddOutfit: "
    @RequiresApi(api = Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_outfit)
        imageView = findViewById<View>(R.id.imView_add_outfit) as ImageView
        picker_btn = findViewById<View>(R.id.button_searchGallery_add_outfit) as Button
        insert_btn = findViewById<View>(R.id.button_insert_add_outfit) as Button
        database = DatabaseManager(this)
        sensitivity_bar = findViewById<View>(R.id.sensitivity_bar_add_outfit) as SeekBar
        sensitivity_bar!!.max = 155
        sensitivity_bar!!.min = 0
        picker_btn!!.setOnClickListener { v ->
            if (v === picker_btn) //start gallery activity
            {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE)
            }
        } //end picker_btn onClick
        insert_btn!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View) {
                val popupMenu = PopupMenu(this@AddOutfitActivity, insert_btn)
                popupMenu.menuInflater.inflate(R.menu.menu_outfit_categories, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                    Toast.makeText(
                        applicationContext, "please wait . . .",
                        Toast.LENGTH_SHORT
                    )
                    var category: String = ""
                    val title = item.title.toString()
                    if ((title == "Top")) {
                        category = "top"
                    }
                    if ((title == "Long Wears")) {
                        category = "long_wears"
                    }
                    if ((title == "Trousers")) {
                        category = "trousers"
                    }
                    if ((title == "Shorts and Skirts")) {
                        category = "shorts_n_skirts"
                    }
                    val bmp = (imageView!!.drawable as BitmapDrawable).bitmap
                    val result = database!!.insertOutfit(category, bmp)
                    if (result) {
                        Toast.makeText(
                            applicationContext,
                            "outfit has added into wardrobe",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Toast.makeText(
                            applicationContext,
                            "outfit can not be added !!",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    false
                } //end onMenuItemClick
                ) //end setOnMenuItemClickListener
                popupMenu.gravity = Gravity.CENTER
                popupMenu.show()
                sensitivity_bar!!.visibility = View.GONE
            }
        }) //end insert_btn onClick
        sensitivity_bar!!.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                val processor = ImageProcessor()
                val processed_bmp = selected_bmp?.let {
                    processor.extractOutfit(
                        it,
                        progress, background_color
                    )
                }
                imageView!!.setImageBitmap(processed_bmp)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        }) //end sensitivity_bar.setOnSeekBarChangeListener
    } //end onCreate

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_IMAGE) {
            when (requestCode) {
                PICK_IMAGE -> {
                    val selectedImage = data!!.data
                    try {
                        val bitmap =
                            MediaStore.Images.Media.getBitmap(this.contentResolver, selectedImage)
                        selected_bmp = bitmap.copy(Bitmap.Config.ARGB_8888, true)
                        imageView!!.setImageBitmap(bitmap)
                        picker_btn!!.visibility = View.GONE
                        insert_btn!!.visibility = View.VISIBLE
                        sensitivity_bar!!.visibility = View.VISIBLE
                    } catch (e: IOException) {
                        Log.d(TAG, "IO exception $e")
                    }
                }
            }
        } //end if statement
    } //End onActivityResult

    companion object {
        private val PICK_IMAGE = 100
    }
} //end activity class
