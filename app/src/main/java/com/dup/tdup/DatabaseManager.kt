package com.dup.tdup

import android.database.sqlite.SQLiteOpenHelper
import com.dup.tdup.DatabaseManager
import android.database.sqlite.SQLiteDatabase
import android.graphics.Bitmap
import android.content.ContentValues
import android.content.Context
import com.dup.tdup.Outfit
import java.io.ByteArrayOutputStream
import java.util.ArrayList

class DatabaseManager  //Constructor
    (context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, 1) {
    private val TABLES = arrayOf("OUTFIT", "MANNEQUIN", "COMBINATION")
    override fun onCreate(db: SQLiteDatabase) {
        //OUTFIT TABLE QUERY
        /*
        * id : outfit id
        * category : category of outfit (lower,upper or full-body)
        * image : bitmap image of outfit
        * */
        val createOutfitTable =
            "CREATE TABLE IF NOT EXISTS OUTFIT (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "category VARCHAR NOT NULL, image BLOB NOT NULL)"
        //MANNEQUIN TABLE QUERY
        /*
         * id : mannequin id
         * image : bitmap image of mannequin
         * */
        val createMannequinTable =
            "CREATE TABLE IF NOT EXISTS MANNEQUIN (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "image BLOB NOT NULL)"
        //COMBINATION TABLE QUERY
        /*
         * id : combination id
         * name : name of combination
         * upper_outfit_id : id of the outfit as upper part of combination
         * lower_outfit_id : " "   "    "     "  lower part of combination
         * full_outfit_id :  " "   "    "     "  full-body part of combination
         * */
        val createCombinationTable =
            "CREATE TABLE IF NOT EXISTS COMBINATION (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "name VARCHAR NOT NULL, " + "upper_outfit_id INTEGER, lower_outfit_id INTEGER, " +
                    "full_outfit_id INTEGER)"
        db.execSQL(createOutfitTable)
        db.execSQL(createMannequinTable)
        db.execSQL(createCombinationTable)
    } //end onCreate

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        for (index in TABLES.indices) {
            val table = TABLES[index]
            db.execSQL("DROP TABLE IF EXISTS $table")
        }
        onCreate(db)
    } //end onUpgrade

    //This function inserts given outfit into database
    fun insertOutfit(category: String?, bmp: Bitmap): Boolean {
        val db = this.writableDatabase
        val stream = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream)
        val img_byte = stream.toByteArray()
        bmp.recycle()
        val values = ContentValues()
        values.put("category", category)
        values.put("image", img_byte)
        val result = db.insert("OUTFIT", null, values)
        db.close()
        return if (result == -1L) false else true
    } //end insertOutfit

    //end getOutfitList
    val outfitList: ArrayList<Outfit>
        get() {
            val outfitList = ArrayList<Outfit>()
            val db = this.writableDatabase
            val cursor = db.rawQuery("SELECT * FROM OUTFIT", null)
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast) {
                    val id = cursor.getInt(cursor.getColumnIndex("id"))
                    val category = cursor.getString(cursor.getColumnIndex("category"))
                    val image = cursor.getBlob(cursor.getColumnIndex("image"))
                    val outfit = Outfit(id, category, image)
                    outfitList.add(outfit)
                    cursor.moveToNext()
                }
            }
            cursor.close()
            db.close()
            return outfitList
        }

    fun getOutfitById(id: Int): Outfit {
        val query = "SELECT * FROM OUTFIT WHERE id = $id"
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)
        val category = cursor.getString(cursor.getColumnIndex("category"))
        val image = cursor.getBlob(cursor.getColumnIndex("image"))
        val outfit = Outfit(id, category, image)
        db.close()
        return outfit
    } //End getOutfitById

    fun deleteOutfitById(id: Int): Int {
        val db = this.writableDatabase
        return db.delete("OUTFIT", "id =? ", arrayOf(id.toString() + ""))
    } //end deleteOutfitById

    companion object {
        private const val DATABASE_NAME = "DATADUP"
    }
} //END CLASS
