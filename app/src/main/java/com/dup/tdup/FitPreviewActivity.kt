package com.dup.tdup

import android.app.Activity
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import android.os.Bundle
import android.util.Log
import com.dup.tdup.R
import com.dup.tdup.PreviewManager
import org.opencv.android.OpenCVLoader

class FitPreviewActivity : Activity() {
    private val mLoaderCallback: BaseLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            when (status) {
                INCOMPATIBLE_MANAGER_VERSION -> return
                INIT_FAILED -> return
                INSTALL_CANCELED -> return
                MARKET_ERROR -> return
                else -> super.onManagerConnected(status)
            }
        }
    } //End mLoaderCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fit_preview)
        if (savedInstanceState == null) {
            fragmentManager
                .beginTransaction()
                .replace(R.id.FrameContainer_fit_preview, PreviewManager.newInstance())
                .commit()
        }
    } //end onCreate

    public override fun onResume() {
        super.onResume()
        if (OpenCVLoader.initDebug()) {
            Log.i("Test Model -- ", "OpenCV initialize success")
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        } else {
            Log.i("Test Model -- ", "OpenCV initialize failed")
        }
    } //End onResume
} //end class
