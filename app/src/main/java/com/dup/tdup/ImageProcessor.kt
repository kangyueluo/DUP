package com.dup.tdup

import android.graphics.Bitmap
import android.util.Log
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.imgproc.Imgproc
import java.util.ArrayList

class ImageProcessor  //Constructor
{
    private val TAG = "C-ImageProcessor"
    fun extractOutfit(bmp: Bitmap, sensitivity_level: Int, colorCode: IntArray): Bitmap {
        //Convert bitmap to mat to process
        val mat = Mat()
        Utils.bitmapToMat(bmp, mat)

        //Gaussian blur
        Imgproc.GaussianBlur(mat, mat, Size(5.0, 5.0), 0.0)
        //convert from bgr to hsv
        val hsv = Mat()
        Imgproc.cvtColor(mat, hsv, Imgproc.COLOR_BGR2HSV)

        //define range of color in HSV
        if (colorCode.size != 3) {
            Log.d(TAG, "colorCode is must contain 3 elements! ! !")
            return bmp
        }
        val code_1 = colorCode[0]
        val code_2 = colorCode[1]
        val code_3 = colorCode[2]
        val lower_range = Scalar(0.0, 0.0, (255 - sensitivity_level).toDouble())
        val upper_range = Scalar(255.0, sensitivity_level.toDouble(), 255.0)
        //threshold the HSV image to get only taget colors
        val mask = Mat()
        Core.inRange(hsv, lower_range, upper_range, mask)
        //bitwise-and mask & original image
        val res = Mat()
        Core.bitwise_and(mat, mat, res, mask)
        //create an inverted mask to segment out the outfit
        val mask_2 = Mat()
        Core.bitwise_not(mask, mask_2)
        //Segmenting the outfit out the frame
        var result = Mat()
        Core.bitwise_and(mat, mat, result, mask_2)

        //Crop only the outfit area
        result = cropAOI(result)

        //convert mat to bitmap
        val result_bmp = Bitmap.createBitmap(
            result.cols(), result.rows(),
            Bitmap.Config.ARGB_8888
        )
        Utils.matToBitmap(result, result_bmp)
        return result_bmp
    } //end extractOutfit

    //This function crop the largest contour in given Mat
    //and returns cropped Mat
    private fun cropAOI(mat: Mat): Mat {
        var largest_area = 0.0
        var largest_contour_index = 0
        var bounding_rect: Rect? = Rect()
        val contours: List<MatOfPoint> = ArrayList()
        val thresh = Mat()
        Imgproc.cvtColor(mat, thresh, Imgproc.COLOR_BGR2GRAY) //grayscale
        Imgproc.threshold(thresh, thresh, 125.0, 255.0, Imgproc.THRESH_BINARY) //threshold

        //find contours
        Imgproc.findContours(
            thresh, contours, thresh, Imgproc.RETR_CCOMP,
            Imgproc.CHAIN_APPROX_SIMPLE
        )
        for (contour_index in contours.indices) {
            val contour_area = Imgproc.contourArea(contours[contour_index])
            if (contour_area > largest_area) {
                largest_area = contour_area
                largest_contour_index = contour_index
                bounding_rect = Imgproc.boundingRect(contours[contour_index])
            } //end if statement
        } //end for loop
        return mat.submat(bounding_rect)
    } //end cropAOI
} //end class
