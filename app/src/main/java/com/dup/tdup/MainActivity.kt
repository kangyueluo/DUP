package com.dup.tdup

import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import android.os.Bundle
import com.dup.tdup.PermissionManager
import com.dup.tdup.R
import android.content.Intent
import android.view.View
import android.widget.Button
import com.dup.tdup.SelectOutfitActivity
import com.dup.tdup.AddOutfitActivity
import org.opencv.android.OpenCVLoader

class MainActivity : AppCompatActivity() {
    private var btn_fit_outfit: Button? = null
    private var btn_add_outfit: Button? = null
    private var textView_add_outfit: TextView? = null
    private var textView_fit_outfit: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        val permissionManager = PermissionManager(this)
        permissionManager.requestPerms()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn_fit_outfit = findViewById<View>(R.id.button_fit_outfit) as Button
        btn_add_outfit = findViewById<View>(R.id.button_add_outfit) as Button
        textView_add_outfit = findViewById<View>(R.id.textview_add_outfit) as TextView
        textView_fit_outfit = findViewById<View>(R.id.text_view_fit_outfit) as TextView
        btn_fit_outfit!!.setOnClickListener { v ->
            if (v === btn_fit_outfit) //start gallery activity
            {
                val intent = Intent(this@MainActivity, SelectOutfitActivity::class.java)
                this@MainActivity.startActivity(intent)
            }
        } //end btn_fit_outfit onClick
        btn_add_outfit!!.setOnClickListener { v ->
            if (v === btn_add_outfit) //start gallery activity
            {
                val intent = Intent(this@MainActivity, AddOutfitActivity::class.java)
                this@MainActivity.startActivity(intent)
            }
        } //end btn_add_outfit onClick
    } //End onCreate

    companion object {
        //Load libs
        init {
            System.loadLibrary("native-lib")
            OpenCVLoader.initDebug()
        }
    }
} //End activity
