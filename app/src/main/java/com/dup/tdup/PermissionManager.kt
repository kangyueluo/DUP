package com.dup.tdup

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.support.v4.content.ContextCompat
import android.content.pm.PackageManager
import android.support.v4.app.ActivityCompat

@SuppressLint("Registered")
class PermissionManager(act: Activity?) : AppCompatActivity() {
    private var act: Activity? = null
    private var allPermissionsGranted = false
    private val permissionList = arrayOf(
        Manifest.permission.CAMERA,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    private val REQUEST_CODE = 999

    init {
        this.act = act
    }

    private fun checkPermissions(): Boolean {
        for (perm in permissionList) {
            if (ContextCompat.checkSelfPermission(
                    act!!,
                    perm
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                allPermissionsGranted = false
                return false
            } else {
                allPermissionsGranted = true
            }
        } //end for loop
        return allPermissionsGranted
    } //end checkPermissions

    fun requestPerms() {
        if (checkPermissions()) return else {
            ActivityCompat.requestPermissions(act!!, permissionList, REQUEST_CODE)
        }
    } //end requestPerms
} //end class
