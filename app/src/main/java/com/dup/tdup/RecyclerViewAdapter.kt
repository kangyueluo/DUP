package com.dup.tdup

import android.content.Context
import com.dup.tdup.Outfit
import android.support.v7.widget.RecyclerView
import com.dup.tdup.RecyclerViewAdapter.MyViewHolder
import android.view.ViewGroup
import android.view.LayoutInflater
import com.dup.tdup.R
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.dup.tdup.ui.DrawView
import android.content.Intent
import com.dup.tdup.FitPreviewActivity
import android.view.View.OnLongClickListener
import com.dup.tdup.RecyclerViewAdapter
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.View.OnCreateContextMenuListener
import android.support.v7.widget.CardView
import android.view.ContextMenu
import android.widget.TextView
import android.view.ContextMenu.ContextMenuInfo
import android.view.View
import android.widget.ImageView
import com.dup.tdup.DatabaseManager

class RecyclerViewAdapter     //Constructor
//end constructor
    (private val mContext: Context, private val outfitList: List<Outfit>) :
    RecyclerView.Adapter<MyViewHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MyViewHolder {
        val view: View
        val mInflater = LayoutInflater.from(mContext)
        view = mInflater.inflate(R.layout.card_view_item, viewGroup, false)
        return MyViewHolder(view)
    } //end onCreateViewHolder

    override fun onBindViewHolder(myViewHolder: MyViewHolder, i: Int) {
        val category = outfitList[i].category
        var description = ""
        if (category == "top") {
            description = "Top"
        }
        if (category == "long_wears") {
            description = "Long Wears"
        }
        if (category == "trousers") {
            description = "Trousers"
        }
        if (category == "shorts_n_skirts") {
            description = "Shorts and Skirts"
        }
        myViewHolder.textView_description.text = description
        val img_byte = outfitList[i].image //get image as byte array
        //convert byte array image to bitmap
        val img_bitmap = BitmapFactory.decodeByteArray(img_byte, 0, img_byte.size)
        myViewHolder.img_item.setImageBitmap(img_bitmap)

        //set click listener
        myViewHolder.cardView_item.setOnClickListener {
            val item_id = outfitList[i].id
            val item_category = outfitList[i].category
            val item_image = outfitList[i].image
            DrawView.currentOutfit = Outfit(item_id, item_category, item_image)
            val intent = Intent(mContext, FitPreviewActivity::class.java)
            mContext.startActivity(intent)
        }
        myViewHolder.cardView_item.setOnLongClickListener {
            selectedOutfitID = outfitList[i].id
            false
        }
    } //end onBindViewHolder

    override fun getItemCount(): Int {
        return outfitList.size
    }

    class MyViewHolder(itemView: View) : ViewHolder(itemView), OnCreateContextMenuListener {
        var cardView_item: CardView
        var textView_description: TextView
        var img_item: ImageView

        init {
            cardView_item = itemView.findViewById<View>(R.id.card_view_item_frame_id) as CardView
            textView_description =
                itemView.findViewById<View>(R.id.card_view_item_description) as TextView
            img_item = itemView.findViewById<View>(R.id.card_view_item_image) as ImageView
            itemView.setOnCreateContextMenuListener(this)
        } //end Constructor

        override fun onCreateContextMenu(menu: ContextMenu, v: View, menuInfo: ContextMenuInfo) {
            val CONTEXT_OPTION_DELETE = 1
            menu.setHeaderTitle("Choose ..")
            val menuDeleteItem = menu.add(this.adapterPosition, CONTEXT_OPTION_DELETE, 0, "Delete")
            menuDeleteItem.setOnMenuItemClickListener {
                val databaseManager = DatabaseManager(itemView.context)
                databaseManager.deleteOutfitById(selectedOutfitID)
                databaseManager.close()
                true
            } //end menuDeleteItem.setOnMenuItemClickListener
        } //end onCreateContextMenu
    } //end MyViewHolder

    companion object {
        var selectedOutfitID = 0
    }
} //End RecyclerViewAdapter
