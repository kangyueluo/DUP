package com.dup.tdup

import android.support.v7.app.AppCompatActivity
import com.dup.tdup.Outfit
import com.dup.tdup.DatabaseManager
import android.os.Bundle
import com.dup.tdup.R
import android.support.v7.widget.RecyclerView
import com.dup.tdup.RecyclerViewAdapter
import android.support.v7.widget.GridLayoutManager
import android.view.View
import java.util.ArrayList

class SelectOutfitActivity : AppCompatActivity() {
    private var outfitList: List<Outfit>? = null
    private var databaseManager: DatabaseManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_outfit)
        databaseManager = DatabaseManager(this)
        outfitList = databaseManager!!.outfitList
        val recyclerView = findViewById<View>(R.id.recycler_view_id) as RecyclerView
        val recyclerViewAdapter = RecyclerViewAdapter(this, outfitList as ArrayList<Outfit>)
        recyclerView.layoutManager = GridLayoutManager(this, 3)
        recyclerView.adapter = recyclerViewAdapter
    } //end onCreate
} //end class
